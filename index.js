import { connect } from 'mongoose' // import package from mongoose to connect to mongoDB to application
import bodyParser from 'body-parser' //body parser is to parse incoming data to json format
import express from 'express' //framework to start a server
import UserModel from './UserModel' //The user model i.e. schema which is the blueprint for a user document
import cors from 'cors' //is a middleware to allow the application (server/API) to be used in a different web application by enabling

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true }) //established a connection to mongodb

const app = express()
app.use(cors()) //enabling server for cross origin resource sharing (cors)
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json()) //Parse body in request to json object

//Api to test if server is live
app.get('/', (req, res) => {
  res.send('Just a test')
})

//Api to retrieve the whole list of users
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

//Api to create a new user
app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

//Edit the details of a specific user
app.put('/users/:id', async (req, res) => {
  try {
    const user = await UserModel.findByIdAndUpdate(
      req.params.id,
      { $set: { ...req.body } },
      { new: true, runValidators: true }
    )
    res.status(200).json(user)
  } catch (err) {
    res.status(500).json(err)
  }
})

//Delete a specific user
app.delete('/users/:id', async (req, res) => {
  try {
    const user = await UserModel.findByIdAndRemove(req.params.id)
    res.status(200).json(user)
  } catch (err) {
    res.status(500).json(err)
  }
})

//Launched server on port 8080
app.listen(8080, () => console.log('Example app listening on port 8080!'))
